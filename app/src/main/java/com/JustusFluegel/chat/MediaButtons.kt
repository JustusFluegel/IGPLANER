@file:Suppress("PackageName")
package com.JustusFluegel.chat

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Typeface
import android.os.Handler
import android.provider.MediaStore
import android.support.v4.content.FileProvider
import android.view.View
import android.widget.Button
import android.widget.Toast
import com.JustusFluegel.emojii.EmojiiButton
import com.JustusFluegel.general.Functions
import com.github.data5tream.emojilib.EmojiEditText
import java.io.File
import java.io.IOException

/*-----------------------------------------
    Buttons implemented yet: camera, choose file
    -----------------------------------------*/


@SuppressLint("Registered")
class MediaButtons(private val currentactivity: Activity, private val documents: Button, private val camera: Button, emojii_button: Button, rootLayout: View, chatText: EmojiEditText, symbols: Typeface) : Activity() {
    val emojiButton: EmojiiButton = EmojiiButton(currentactivity, emojii_button, chatText, rootLayout)

    init {

        emojii_button.setOnLongClickListener {
            Global.openemoji = false

            documents.visibility = View.VISIBLE

            if (currentactivity.applicationContext.packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {

                camera.visibility = View.VISIBLE

            }

            false
        }

        camera.setOnClickListener {
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (takePictureIntent.resolveActivity(currentactivity.packageManager) != null) {

                @Suppress("LocalVariableName", "UNUSED_VARIABLE")
                val picture_path = dispatchTakePictureIntent()

                TODO("Add usage for pictures")

            }
        }

        documents.setOnClickListener {
            val fileopendialog = Intent()
                    .setType("*/*")
                    .setAction(Intent.ACTION_GET_CONTENT)

            currentactivity.startActivityForResult(Intent.createChooser(fileopendialog, "Datei Auswählen"), 123)
        }

        emojii_button.typeface = symbols
        documents.typeface = symbols
        camera.typeface = symbols
    }

    fun requestOnUserInteraction() {

        val handler = Handler()

        handler.postDelayed({
            documents.visibility = View.INVISIBLE
            camera.visibility = View.INVISIBLE
        }, 20)

    }

    fun requestOnActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        if (requestCode == 123 && resultCode == Activity.RESULT_OK) {

            val selectedFile = data.data

            Toast.makeText(currentactivity.applicationContext, selectedFile!!.toString(), Toast.LENGTH_LONG).show()

        } else if (requestCode == 122 && resultCode == Activity.RESULT_OK) {

            val extras = data.extras
            val imageBitmap = extras!!.get("data") as Bitmap

            Toast.makeText(currentactivity.applicationContext, imageBitmap.toString(), Toast.LENGTH_LONG).show()

        }
    }

    private fun dispatchTakePictureIntent(): String {
        var photoFile: File? = null

        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent.resolveActivity(currentactivity.packageManager) != null) {

            try {
                photoFile = Functions.createImageFile(currentactivity)
            } catch (ignored: IOException) {

            }

            if (photoFile != null) {
                val photoURI = FileProvider.getUriForFile(currentactivity,
                        "com.example.android.fileprovider",
                        photoFile)
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                currentactivity.startActivityForResult(takePictureIntent, 121)
            }

        }

        return photoFile!!.absolutePath
    }

}
