@file:Suppress("PackageName")
package com.JustusFluegel.igplaner

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MotionEvent
import android.view.View
import android.widget.TextView

import com.JustusFluegel.general.Functions
import com.JustusFluegel.general.Menue
import com.github.amlcurran.showcaseview.OnShowcaseEventListener
import com.github.amlcurran.showcaseview.ShowcaseView
import com.github.amlcurran.showcaseview.targets.ViewTarget

class StandardActivity : AppCompatActivity() {

    private lateinit var showCaseViewExtender: ShowcaseView.Builder
    private lateinit var showCaseViewDienste: ShowcaseView.Builder
    private lateinit var showCaseViewStundenplan: ShowcaseView.Builder
    private lateinit var showCaseViewAnmelden: ShowcaseView.Builder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_standard)

        Menue.Menu(this@StandardActivity, true)

        showShowcaseViewExtender()

    }

    override fun onResume() {
        super.onResume()

        (findViewById<View>(R.id.Wlkommensgruß) as TextView).setTextColor(Functions.getDesignTextcolor(this@StandardActivity))
        findViewById<View>(R.id.standardActivity_background).setBackgroundResource(Functions.getDesignBackgroundImage(this@StandardActivity))

        Menue.ApplyColors(this@StandardActivity)

    }


    fun showShowcaseViewAnmelden() {

        showCaseViewAnmelden = ShowcaseView.Builder(this)
                .withMaterialShowcase()
                .setTarget(ViewTarget(R.id.AnmeldeIcon, this))
                .setContentTitle("Anmelden")
                .setContentText("Klicke auf diesen Button, um dich mit deinem Account, den du von Justus bekommst, anzumelden")
                .setStyle(R.style.ShowCaseViewStyle)
                .setShowcaseEventListener(object : OnShowcaseEventListener {
                    override fun onShowcaseViewHide(showcaseView: ShowcaseView) {}

                    override fun onShowcaseViewDidHide(showcaseView: ShowcaseView) {

                        startActivity(Intent(this@StandardActivity, Anmelden::class.java))

                    }

                    override fun onShowcaseViewShow(showcaseView: ShowcaseView) {

                    }

                    override fun onShowcaseViewTouchBlocked(motionEvent: MotionEvent) {

                    }

                })

        showCaseViewAnmelden.build()

    }

    fun showShowcaseViewStundenplan() {

        showCaseViewStundenplan = ShowcaseView.Builder(this)
                .withMaterialShowcase()
                .setTarget(ViewTarget(R.id.StundenplanIcon, this))
                .setContentTitle("Stundenplan")
                .setContentText("Klicke auf diesen Button, um dir deinen Studenplan anzeigen zu lassen.")
                .setStyle(R.style.ShowCaseViewStyle)
                //.singleShot(1)
                .setShowcaseEventListener(object : OnShowcaseEventListener {
                    override fun onShowcaseViewHide(showcaseView: ShowcaseView) {

                        showShowcaseViewAnmelden()

                    }

                    override fun onShowcaseViewDidHide(showcaseView: ShowcaseView) {

                    }

                    override fun onShowcaseViewShow(showcaseView: ShowcaseView) {

                    }

                    override fun onShowcaseViewTouchBlocked(motionEvent: MotionEvent) {

                    }

                })

        showCaseViewStundenplan.build()

    }

    fun showShowcaseViewDienste() {

        showCaseViewDienste = ShowcaseView.Builder(this)
                .withMaterialShowcase()
                .setTarget(ViewTarget(R.id.DiensteIcon, this))
                .setContentTitle("Dienste")
                .setContentText("Klicke auf diesen Button, um die Aktuellen Dienste deiner Klasse anzuzeigen. Sie werden automatisch jede Woche neu generiert.")
                .setStyle(R.style.ShowCaseViewStyle)
                //.singleShot(1)
                .setShowcaseEventListener(object : OnShowcaseEventListener {
                    override fun onShowcaseViewHide(showcaseView: ShowcaseView) {

                        showShowcaseViewStundenplan()

                    }

                    override fun onShowcaseViewDidHide(showcaseView: ShowcaseView) {

                    }

                    override fun onShowcaseViewShow(showcaseView: ShowcaseView) {

                    }

                    override fun onShowcaseViewTouchBlocked(motionEvent: MotionEvent) {

                    }

                })

        showCaseViewDienste.build()

    }

    private fun showShowcaseViewExtender() {

        showCaseViewExtender = ShowcaseView.Builder(this)
                .withMaterialShowcase()
                .setTarget(ViewTarget(R.id.MenuButton, this))
                .setContentTitle("Erweitern")
                .setContentText("Klicke auf diesen Button, um das Menü zu vergrößern und alle Texte anzuzeigen.")
                .setStyle(R.style.ShowCaseViewStyle)
                .singleShot(708180739)
                .setShowcaseEventListener(object : OnShowcaseEventListener {
                    override fun onShowcaseViewHide(showcaseView: ShowcaseView) {

                        showShowcaseViewDienste()

                    }

                    override fun onShowcaseViewDidHide(showcaseView: ShowcaseView) {

                    }

                    override fun onShowcaseViewShow(showcaseView: ShowcaseView) {

                    }

                    override fun onShowcaseViewTouchBlocked(motionEvent: MotionEvent) {

                    }

                })

        showCaseViewExtender.build()

    }

}
