@file:Suppress("PackageName", "LocalVariableName", "FunctionName")
package com.JustusFluegel.igplaner

import android.content.Intent
import android.content.res.Configuration
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.DisplayMetrics
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.TextView

import com.JustusFluegel.general.Functions
import com.JustusFluegel.general.Menue
import com.JustusFluegel.popups.Dialog_Stundenplan
import com.github.amlcurran.showcaseview.OnShowcaseEventListener
import com.github.amlcurran.showcaseview.ShowcaseView
import com.github.amlcurran.showcaseview.targets.ViewTarget

class StundenplanProitrait : AppCompatActivity() {

    private var day = "MONDAY"

    private lateinit var showCaseViewTage: ShowcaseView.Builder
    private lateinit var showCaseViewFach: ShowcaseView.Builder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stundenplan_proitrait)

        findViewById<View>(R.id.TagesAuswahlButtons).setBackgroundColor(Functions.getDesignBackground(this@StundenplanProitrait))
        (findViewById<View>(R.id.MonatagButton) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanProitrait))
        (findViewById<View>(R.id.DienstagButton) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanProitrait))
        (findViewById<View>(R.id.MittwochButton) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanProitrait))
        (findViewById<View>(R.id.DonnerstagButton) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanProitrait))
        (findViewById<View>(R.id.FreitagButton) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanProitrait))

        (findViewById<View>(R.id.StundenplanUberschrift) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanProitrait))
        findViewById<View>(R.id.StundenplanUberschrift).setBackgroundColor(Functions.getDesignBackground(this@StundenplanProitrait))
        (findViewById<View>(R.id.Stundenplan1Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanProitrait))
        (findViewById<View>(R.id.Stundenplan2Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanProitrait))
        (findViewById<View>(R.id.Stundenplan3Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanProitrait))
        (findViewById<View>(R.id.Stundenplan4Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanProitrait))
        (findViewById<View>(R.id.Stundenplan5Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanProitrait))
        (findViewById<View>(R.id.Stundenplan6Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanProitrait))
        (findViewById<View>(R.id.Stundenplan7Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanProitrait))
        (findViewById<View>(R.id.Stundenplan8Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanProitrait))
        (findViewById<View>(R.id.Stundenplan9Stunde) as TextView).setTextColor(Functions.getDesignTextcolor(this@StundenplanProitrait))

        findViewById<View>(R.id.StundenplanContent).setBackgroundResource(Functions.getDesignBackgroundImage(this@StundenplanProitrait))


        //<editor-fold desc="Displayabhaenig aendern">
        if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            startActivity(Intent(this@StundenplanProitrait, StundenplanLandscape::class.java))
            finish()
        }
        //</editor-fold>

        //<editor-fold desc="Variablen">
        val Montagbutton = findViewById<Button>(R.id.MonatagButton)
        val DienstagButton = findViewById<Button>(R.id.DienstagButton)
        val MittwochButton = findViewById<Button>(R.id.MittwochButton)
        val DonnerstagButton = findViewById<Button>(R.id.DonnerstagButton)
        val FreitagButton = findViewById<Button>(R.id.FreitagButton)


        val Uberschrift = findViewById<TextView>(R.id.StundenplanUberschrift)
        val Stunde1 = findViewById<TextView>(R.id.Stundenplan1Stunde)
        val Stunde2 = findViewById<TextView>(R.id.Stundenplan2Stunde)
        val Stunde3 = findViewById<TextView>(R.id.Stundenplan3Stunde)
        val Stunde4 = findViewById<TextView>(R.id.Stundenplan4Stunde)
        val Stunde5 = findViewById<TextView>(R.id.Stundenplan5Stunde)
        val Stunde6 = findViewById<TextView>(R.id.Stundenplan6Stunde)
        val Stunde7 = findViewById<TextView>(R.id.Stundenplan7Stunde)
        val Stunde8 = findViewById<TextView>(R.id.Stundenplan8Stunde)
        val Stunde9 = findViewById<TextView>(R.id.Stundenplan9Stunde)
        //</editor-fold>

        SetTimetableToDay(1, Uberschrift, Stunde1, Stunde2, Stunde3, Stunde4, Stunde5, Stunde6, Stunde7, Stunde8, Stunde9)
        Montagbutton.setTextColor(ContextCompat.getColor(applicationContext,R.color.hervorgehoben))

        //<editor-fold desc="Buttongröße anpassen">
        val metrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(metrics)

        val heightforelements = metrics.heightPixels / 5

        Montagbutton.height = heightforelements
        DienstagButton.height = heightforelements
        MittwochButton.height = heightforelements
        DonnerstagButton.height = heightforelements
        FreitagButton.height = heightforelements
        //</editor-fold>

        Menue.Menu(this@StundenplanProitrait, false)

        //<editor-fold desc="Tagesbuttons">

        Montagbutton.setOnClickListener {
            if (day == "MONDAY") {
                val d = Dialog_Stundenplan(this@StundenplanProitrait)
                d.window!!.setBackgroundDrawable(ColorDrawable(ContextCompat.getColor(applicationContext,R.color.tr_background)))
                d.show()

            }

            day = "MONDAY"
            SetTimetableToDay(1, Uberschrift, Stunde1, Stunde2, Stunde3, Stunde4, Stunde5, Stunde6, Stunde7, Stunde8, Stunde9)
            Montagbutton.setTextColor(ContextCompat.getColor(applicationContext,R.color.hervorgehoben))
            DienstagButton.setTextColor(Functions.getDesignTextcolor(this@StundenplanProitrait))
            MittwochButton.setTextColor(Functions.getDesignTextcolor(this@StundenplanProitrait))
            DonnerstagButton.setTextColor(Functions.getDesignTextcolor(this@StundenplanProitrait))
            FreitagButton.setTextColor(Functions.getDesignTextcolor(this@StundenplanProitrait))
        }

        DienstagButton.setOnClickListener {
            if (day == "TUESDAY") {
                val d = Dialog_Stundenplan(this@StundenplanProitrait)
                d.window!!.setBackgroundDrawable(ColorDrawable(ContextCompat.getColor(applicationContext,R.color.tr_background)))
                d.show()
            }

            day = "TUESDAY"
            SetTimetableToDay(2, Uberschrift, Stunde1, Stunde2, Stunde3, Stunde4, Stunde5, Stunde6, Stunde7, Stunde8, Stunde9)
            Montagbutton.setTextColor(Functions.getDesignTextcolor(this@StundenplanProitrait))
            DienstagButton.setTextColor(ContextCompat.getColor(applicationContext,R.color.hervorgehoben))
            MittwochButton.setTextColor(Functions.getDesignTextcolor(this@StundenplanProitrait))
            DonnerstagButton.setTextColor(Functions.getDesignTextcolor(this@StundenplanProitrait))
            FreitagButton.setTextColor(Functions.getDesignTextcolor(this@StundenplanProitrait))
        }

        MittwochButton.setOnClickListener {
            if (day == "WEDNESDAY") {
                val d = Dialog_Stundenplan(this@StundenplanProitrait)
                d.window!!.setBackgroundDrawable(ColorDrawable(ContextCompat.getColor(applicationContext,R.color.tr_background)))
                d.show()
            }

            day = "WEDNESDAY"
            SetTimetableToDay(3, Uberschrift, Stunde1, Stunde2, Stunde3, Stunde4, Stunde5, Stunde6, Stunde7, Stunde8, Stunde9)
            Montagbutton.setTextColor(Functions.getDesignTextcolor(this@StundenplanProitrait))
            DienstagButton.setTextColor(Functions.getDesignTextcolor(this@StundenplanProitrait))
            MittwochButton.setTextColor(ContextCompat.getColor(applicationContext,R.color.hervorgehoben))
            DonnerstagButton.setTextColor(Functions.getDesignTextcolor(this@StundenplanProitrait))
            FreitagButton.setTextColor(Functions.getDesignTextcolor(this@StundenplanProitrait))
        }

        DonnerstagButton.setOnClickListener {
            if (day == "THURSDAY") {
                val d = Dialog_Stundenplan(this@StundenplanProitrait)
                d.window!!.setBackgroundDrawable(ColorDrawable(ContextCompat.getColor(applicationContext,R.color.tr_background)))
                d.show()
            }

            day = "THURSDAY"
            SetTimetableToDay(4, Uberschrift, Stunde1, Stunde2, Stunde3, Stunde4, Stunde5, Stunde6, Stunde7, Stunde8, Stunde9)
            Montagbutton.setTextColor(Functions.getDesignTextcolor(this@StundenplanProitrait))
            DienstagButton.setTextColor(Functions.getDesignTextcolor(this@StundenplanProitrait))
            MittwochButton.setTextColor(Functions.getDesignTextcolor(this@StundenplanProitrait))
            DonnerstagButton.setTextColor(ContextCompat.getColor(applicationContext,R.color.hervorgehoben))
            FreitagButton.setTextColor(Functions.getDesignTextcolor(this@StundenplanProitrait))
        }

        FreitagButton.setOnClickListener {
            if (day == "FRIDAY") {
                val d = Dialog_Stundenplan(this@StundenplanProitrait)
                d.window!!.setBackgroundDrawable(ColorDrawable(ContextCompat.getColor(applicationContext,R.color.tr_background)))
                d.show()
            }

            day = "FRIDAY"
            SetTimetableToDay(5, Uberschrift, Stunde1, Stunde2, Stunde3, Stunde4, Stunde5, Stunde6, Stunde7, Stunde8, Stunde9)
            Montagbutton.setTextColor(Functions.getDesignTextcolor(this@StundenplanProitrait))
            DienstagButton.setTextColor(Functions.getDesignTextcolor(this@StundenplanProitrait))
            MittwochButton.setTextColor(Functions.getDesignTextcolor(this@StundenplanProitrait))
            DonnerstagButton.setTextColor(Functions.getDesignTextcolor(this@StundenplanProitrait))
            FreitagButton.setTextColor(ContextCompat.getColor(applicationContext,R.color.hervorgehoben))
        }

        //</editor-fold>

        showShowcaseViewTage()


    }

    fun showShowcaseViewFach() {

        showCaseViewFach = ShowcaseView.Builder(this)
                .withMaterialShowcase()
                .setTarget(ViewTarget(R.id.Stundenplan5Stunde, this))
                .setContentTitle("Fach")
                .setContentText("Hier Kannst du dein Fach ablesen.")
                .setStyle(R.style.ShowCaseViewStyle)
                .setShowcaseEventListener(object : OnShowcaseEventListener {
                    override fun onShowcaseViewHide(showcaseView: ShowcaseView) {

                        //nächster Showcase

                    }

                    override fun onShowcaseViewDidHide(showcaseView: ShowcaseView) {

                    }

                    override fun onShowcaseViewShow(showcaseView: ShowcaseView) {

                    }

                    override fun onShowcaseViewTouchBlocked(motionEvent: MotionEvent) {

                    }

                })

        showCaseViewFach.build()

    }

    private fun showShowcaseViewTage() {

        showCaseViewTage = ShowcaseView.Builder(this)
                .withMaterialShowcase()
                .setTarget(ViewTarget(R.id.MittwochButton, this))
                .setContentTitle("Tage")
                .setContentText("Tippe hier, um zwischen den Tagen zu wechseln.")
                .setStyle(R.style.ShowCaseViewStyle)
                .singleShot(755815493)
                .setShowcaseEventListener(object : OnShowcaseEventListener {
                    override fun onShowcaseViewHide(showcaseView: ShowcaseView) {

                        showShowcaseViewFach()

                    }

                    override fun onShowcaseViewDidHide(showcaseView: ShowcaseView) {

                    }

                    override fun onShowcaseViewShow(showcaseView: ShowcaseView) {

                    }

                    override fun onShowcaseViewTouchBlocked(motionEvent: MotionEvent) {

                    }

                })

        showCaseViewTage.build()

    }

    private fun SetTimetableToDay(day: Int, S0: TextView, S1: TextView, S2: TextView, S3: TextView, S4: TextView, S5: TextView, S6: TextView, S7: TextView, S8: TextView, S9: TextView) {

        when (day) {
            1 -> {

                S0.text = "Montag"

                S1.text = StundenplanLandscape.Montag[0]
                S2.text = StundenplanLandscape.Montag[1]
                S3.text = StundenplanLandscape.Montag[2]
                S4.text = StundenplanLandscape.Montag[3]
                S5.text = StundenplanLandscape.Montag[4]
                S6.text = StundenplanLandscape.Montag[5]
                S7.text = StundenplanLandscape.Montag[6]
                S8.text = StundenplanLandscape.Montag[7]
                S9.text = StundenplanLandscape.Montag[8]
            }

            2 -> {

                S0.text = "Dienstag"

                S1.text = StundenplanLandscape.Dienstag[0]
                S2.text = StundenplanLandscape.Dienstag[1]
                S3.text = StundenplanLandscape.Dienstag[2]
                S4.text = StundenplanLandscape.Dienstag[3]
                S5.text = StundenplanLandscape.Dienstag[4]
                S6.text = StundenplanLandscape.Dienstag[5]
                S7.text = StundenplanLandscape.Dienstag[6]
                S8.text = StundenplanLandscape.Dienstag[7]
                S9.text = StundenplanLandscape.Dienstag[8]
            }

            3 -> {

                S0.text = "Mittwoch"

                S1.text = StundenplanLandscape.Mittwoch[0]
                S2.text = StundenplanLandscape.Mittwoch[1]
                S3.text = StundenplanLandscape.Mittwoch[2]
                S4.text = StundenplanLandscape.Mittwoch[3]
                S5.text = StundenplanLandscape.Mittwoch[4]
                S6.text = StundenplanLandscape.Mittwoch[5]
                S7.text = StundenplanLandscape.Mittwoch[6]
                S8.text = StundenplanLandscape.Mittwoch[7]
                S9.text = StundenplanLandscape.Mittwoch[8]
            }

            4 -> {

                S0.text = "Donnerstag"

                S1.text = StundenplanLandscape.Donnerstag[0]
                S2.text = StundenplanLandscape.Donnerstag[1]
                S3.text = StundenplanLandscape.Donnerstag[2]
                S4.text = StundenplanLandscape.Donnerstag[3]
                S5.text = StundenplanLandscape.Donnerstag[4]
                S6.text = StundenplanLandscape.Donnerstag[5]
                S7.text = StundenplanLandscape.Donnerstag[6]
                S8.text = StundenplanLandscape.Donnerstag[7]
                S9.text = StundenplanLandscape.Donnerstag[8]
            }

            5 -> {

                S0.text = "Freitag"

                S1.text = StundenplanLandscape.Freitag[0]
                S2.text = StundenplanLandscape.Freitag[1]
                S3.text = StundenplanLandscape.Freitag[2]
                S4.text = StundenplanLandscape.Freitag[3]
                S5.text = StundenplanLandscape.Freitag[4]
                S6.text = StundenplanLandscape.Freitag[5]
                S7.text = StundenplanLandscape.Freitag[6]
                S8.text = StundenplanLandscape.Freitag[7]
                S9.text = StundenplanLandscape.Freitag[8]
            }
        }


    }
}
