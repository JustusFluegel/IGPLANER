@file:Suppress("PackageName")
package com.JustusFluegel.general

import android.app.Activity

class WasOpened(private val currentact: Activity) {

    var opened: Boolean?
        get() {

            val pref = currentact.getSharedPreferences("Opened", 0)
            return pref.getBoolean("opened", false)
        }
        set(opened) {

            val pref = currentact.getSharedPreferences("Opened", 0).edit()
            pref.putBoolean("opened", opened!!)
            pref.apply()

        }

}
