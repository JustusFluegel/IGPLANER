@file:Suppress("PackageName")
package com.JustusFluegel.html

import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder
import java.util.*
import kotlin.collections.ArrayList

class ServerHttpConnection(private var serverurl: String?) {

    @Throws(IOException::class)
    private fun newConnection(url: String?): HttpURLConnection {
        val server = URL(url)

        val conn = server.openConnection() as HttpURLConnection
        conn.doOutput = true
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded")
        conn.setRequestProperty("User-Agent", "Mozilla/5.0 ( compatible ) ")
        conn.setRequestProperty("Accept", "*/*")

        return conn

    }

    @Throws(IOException::class)
    fun sendTextforResult(adress: String, text: String): ArrayList<String> {

        val conn = newConnection(serverurl)
        val send = adress + "=" + URLEncoder.encode(text, "UTF-8")
        conn.setFixedLengthStreamingMode(send.toByteArray().size)

        val writer = OutputStreamWriter(conn.outputStream)
        writer.write(send)
        writer.flush()
        writer.close()

        val answer = conn.inputStream

        val reader = BufferedReader(InputStreamReader(answer))
        val lines = ArrayList<String>()
        lines.addAll(reader.readLines())
        answer.close()

        return lines

    }

    fun arraylisttomultiplelinesstring(list: ArrayList<String>): String {
        val builder = StringBuilder()

        for (collumn in list) {
            builder.append(collumn)
            builder.append("\n")
        }

        return builder.toString().trim { it <= ' ' }
    }

    fun changeURL(url: String) {
        serverurl = url
    }

    @Throws(Exception::class)
    fun sendmultipleTextForResult(adresses: ArrayList<String>, textes: ArrayList<String>): ArrayList<String> {

        if (adresses.size != textes.size) {
            throw Exception("Lists haven't equal size")

        } else {

            val conn = newConnection(serverurl)

            val sendbuilder = StringBuilder()

            for (i in adresses.indices) {

                sendbuilder.append(adresses[i])
                sendbuilder.append("=")
                sendbuilder.append(URLEncoder.encode(textes[i], "UTF-8"))
                if (adresses.size - 1 != i) {
                    sendbuilder.append("&")
                }

            }

            val send = sendbuilder.toString()
            conn.setFixedLengthStreamingMode(send.toByteArray().size)

            val writer = OutputStreamWriter(conn.outputStream)
            writer.write(send)
            writer.flush()
            writer.close()

            val answer = conn.inputStream

            val reader = BufferedReader(InputStreamReader(answer))
            val lines = ArrayList<String>()
            lines.addAll(reader.readLines())
            answer.close()

            return lines

        }

    }

    @Throws(Exception::class)
    fun sendmultipleTextForResultinArray(adresses: Array<String>, textes: Array<String>): ArrayList<String> {

        return sendmultipleTextForResult(ArrayList(Arrays.asList(*adresses)), ArrayList(Arrays.asList(*textes)))

    }

}
