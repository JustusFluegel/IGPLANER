@file:Suppress("PackageName", "PrivatePropertyName", "LocalVariableName", "FunctionName")
package com.JustusFluegel.dienste

import android.app.Activity
import android.util.Log
import com.JustusFluegel.JDBC.JdbcConnection
import com.JustusFluegel.igplaner.Variables
import java.security.SecureRandom
import java.sql.SQLException
import java.util.*

class Dienste(activity: Activity) {

    private val Dienstenamen = ArrayList<String>()
    private val DienstePersonenAnzahl = ArrayList<Int>()
    private val staticPersons = ArrayList<String>()
    private val anotherStaticPerson = ArrayList<String>()
    private val overlap = ArrayList<Boolean>()
    private val TimestampWeek = ArrayList<Double>()
    private val actualdienste = ArrayList<String>()


    private val diensteconnection = JdbcConnection(Variables.SqlUsername, Variables.SqlPassword, Variables.SqlDomain, "dienste")
    private val klassenconnection = JdbcConnection(Variables.SqlUsername, Variables.SqlPassword, Variables.SqlDomain, "klasseninformation")

    private val weekTimestamp: Double
        get() {
            val Time = System.currentTimeMillis() - 345600000
            val umrechnungszahl = 604800000

            return ((Time - Time % umrechnungszahl) / umrechnungszahl).toDouble()
        }

    private val persons: ArrayList<ArrayList<String>>
        @Throws(SQLException::class)
        get() {
            val result = ArrayList<ArrayList<String>>()
            result.add(ArrayList())
            result.add(ArrayList())

            val res = klassenconnection.runSQL("SELECT * FROM " + Variables.klassen[Variables.Klasse] + "_dienste")

            while (res!!.next()) {

                result[0].add(res.getString("Benutzername"))
                result[1].add(res.getString("Dienste"))

            }

            return result
        }


    init {

        Thread(Runnable {
            dienste.clear()

            try {

                val randoms = ArrayList<Int>()

                val list = sortPersons(persons)

                Log.d("Persons", list.toString())

                getDiensteInfos()

                val Personindexone = getMinPersons(list, 0)

                var minPersons = 0

                for (i in Dienstenamen.indices) {

                    if (!overlap[i]) {

                        minPersons += DienstePersonenAnzahl[i]

                    }

                }

                if (minPersons <= list[0].size) {

                    for (i in Dienstenamen.indices) {
                        if (TimestampWeek[i] != weekTimestamp) {

                            val nCSV = ArrayList<String>()

                            if (Personindexone.size >= DienstePersonenAnzahl[i]) {


                                for (o in 0 until DienstePersonenAnzahl[i]) {

                                    val random = getRandomInt(0, Personindexone.size)

                                    randoms.add(random)

                                    nCSV.add(Personindexone[random])
                                    Personindexone.removeAt(random)


                                }

                                getRandomInt(0, Personindexone.size)

                            } else {


                            }

                            val CSV = StringBuilder()

                            for (o in nCSV.indices) {

                                if (o == 0) {

                                    CSV.append(nCSV[o])


                                } else {

                                    CSV.append(" , ").append(nCSV[o])

                                }

                            }

                            dienste.add(CSV.toString())

                        } else {

                            dienste = actualdienste

                        }
                    }
                } else {

                    Log.e("IGPlaner", "To less persons for services: " + minPersons.toString())
                    activity.finish()

                }

                Log.d("Dienste", dienste.toString())
                Log.d("Randoms", randoms.toString())
                Log.d("Dienste.Size", dienste.size.toString())


            } catch (e: Exception) {
                e.printStackTrace()
            }
        }).start()
    }

    @Throws(SQLException::class)
    private fun getDiensteInfos() {

        val res = diensteconnection.runSQL("SELECT * FROM " + Variables.klassen[Variables.Klasse] + "_info")

        while (res!!.next()) {

            Dienstenamen.add(res.getString("Dienstname"))
            TimestampWeek.add(res.getDouble("TimestampWeek"))


        }

        val res2 = diensteconnection.runSQL("SELECT * FROM " + Variables.klassen[Variables.Klasse] + "")

        while (res2!!.next()) {

            for (dienstname in Dienstenamen) {

                if (dienstname == res2.getString("Dienstname")) {

                    DienstePersonenAnzahl.add(res2.getInt("Personenanzahl"))
                    staticPersons.add(res2.getString("statischePerson"))
                    anotherStaticPerson.add(res2.getString("alternativeStatischePerson"))
                    overlap.add(res2.getBoolean("overlap"))
                }

            }

        }

    }

    @Throws(NumberFormatException::class)
    private fun sortPersons(persons: ArrayList<ArrayList<String>>): ArrayList<ArrayList<String>> {
        val result = ArrayList<ArrayList<String>>()
        result.add(ArrayList())
        result.add(ArrayList())

        var minNumber = Integer.MAX_VALUE

        for (services in persons[1]) {

            val Services = Integer.valueOf(services)

            if (Services < minNumber) {
                minNumber = Services
            }

        }

        while (persons[0].size != 0) {

            var i = 0
            while (i < persons[0].size) {

                if (Integer.valueOf(persons[1][i]) == minNumber) {

                    result[0].add(persons[0][i])
                    result[1].add(persons[1][i])

                    persons[0].removeAt(i)
                    persons[1].removeAt(i)

                    i--

                }
                i++

            }

            minNumber++

        }

        return result
    }

    @Throws(NumberFormatException::class, SQLException::class)
    private fun PersonUserNamesToVisibleNames(persons: ArrayList<ArrayList<String>>): ArrayList<ArrayList<String>> {
        val result = ArrayList<ArrayList<String>>()
        result.add(ArrayList())
        result.add(persons[1])

        for (username in persons[0]) {

            result[0].add(UsernameToVisibleName(username))

        }

        return result
    }

    @Throws(SQLException::class)
    private fun UsernameToVisibleName(Username: String): String {

        val res = klassenconnection.runSQL("SELECT Anzeigename FROM " + Variables.klassen[Variables.Klasse] + " WHERE Benutzername='" + Username + "'")

        res!!.next()

        return res.getString("Anzeigename")

    }

    @Throws(NumberFormatException::class)
    private fun getMinPersons(persons: ArrayList<ArrayList<String>>, index: Int): ArrayList<String> {

        val result = ArrayList<String>()

        var minValue = Integer.MAX_VALUE

        for (i in 0 until persons[0].size) {

            if (Integer.valueOf(persons[1][i]) < minValue) {

                minValue = Integer.valueOf(persons[1][i])

            }

        }

        for (i in 0 until persons[0].size) {

            if (Integer.valueOf(persons[1][i]) == minValue + index) {

                result.add(persons[0][i])

            }

        }

        return result

    }

    private fun getRandomInt(minimum: Int, maximum: Int): Int {

        val rand = SecureRandom()

        return if (maximum - minimum == 0) {

            0

        } else {
            rand.nextInt(maximum - minimum) + minimum

        }
    }

    companion object {

        private var dienste = ArrayList<String>()
    }

}
